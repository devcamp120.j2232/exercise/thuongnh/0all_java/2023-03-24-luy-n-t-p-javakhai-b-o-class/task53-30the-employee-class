import models.*;

public class App {
    public static void main(String[] args) throws Exception {
        
        // khởi tạo 1 đối tượng  nguyễn hữu thương 
        Employee nhThuong  =  new Employee(1001, "thuong", "nguyen huu", 5000);
        // khởi tạo đối tượng lê thùy dương 
        
        Employee leThuyDuong  =  new Employee(1009, "Dương", "Lê Thùy ", 3000);
        //System.out.println();
        System.out.println(">>>>>>>>nguyễn hữu thương "); 
        System.out.println(nhThuong.getName());
        System.out.println(nhThuong.getAnnualSalary());
        System.out.println(nhThuong.raiseSalary(25f));
        System.out.println(nhThuong.toString());

        System.out.println(">>>>>>>lê thùy dương  ");
        System.out.println(leThuyDuong.getName());
        System.out.println(leThuyDuong.getAnnualSalary());
        System.out.println(leThuyDuong.raiseSalary(25f));
        System.out.println(leThuyDuong.toString());
        

    }
}
