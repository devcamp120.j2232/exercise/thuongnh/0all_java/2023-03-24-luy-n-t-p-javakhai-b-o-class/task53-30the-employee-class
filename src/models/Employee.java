package models;

public class Employee {

    private int id;
    private String firstName;
    private String lastName;
    private  float salary;   // lương 1 tháng 
    

    //  phương thức khởi tạo 
    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }


    public int getId() {
        return id;
    }


    public String getFirstName() {
        return firstName;
    }


    public String getLastName() {
        return lastName;
    }


    public float getSalary() {
        return salary;
    }
    // trả về tên đầy đủ
    public String getName() {
        return   this.firstName + " " + this.lastName;
    }
    //  trả về lương của 1 năm
    public float getAnnualSalary() {
        return salary * 12;
    }

    public float raiseSalary(Float percent)  {
        return this.salary + (this.salary * (percent / 100));
    }


    @Override
    public String toString() {
        return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", salary=" + salary
                + "]";
    }

    



    

    // get và set 
    
    
    
    
}
